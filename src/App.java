public class App {
    public static void main(String[] args) throws Exception {
        // phần khai báo 
        int[] number1 = {1, 5, 10}; 
        int[] number2 = {1,2,3,5,7,9};

        // yêu cầu 2 
        System.out.println("yêu cầu 2");
        System.out.println(sumNumbersV1());
        // yêu cầu 3
        System.out.println("yêu cầu 3");
        System.out.println(sumNumbersV2(number1));
        System.out.println( sumNumbersV2(number2));
        // yêu cầu 4
        System.out.println("yêu cầu 4");
        printHello(20);
        printHello(91);
        // yêu cầu 5
    }
    public static int sumNumbersV1() {
        int sum = 0;
        for (int i = 0;i < 100;i++) {
            sum = sum + i;
        }
        return sum;
    }
    public static int sumNumbersV2(int[]  input) {
        int sum = 0;
        for(int i = 0; i < input.length; i++){
            sum = sum + input[i];
        }
        return sum;
    }
    

    public static void printHello(int  paramA) {
        
        if(paramA % 2 == 0){
            System.out.println(paramA +   "day la so chan" );

        }else{
            System.out.println(paramA + "day la so le" );
        }
    }
}
