package goi;
public class Task5010 {
    public static void main(String[] args) throws Exception {
        Task5010 task = new Task5010();

        //đây là method thường (non-static)
        int total = task.sumNumbersV1();
        System.out.println("Hello, World! Total from 1 to 100 is: " +total);
        
         //đây là method static
        total = Task5010.sumNumbersV1a();
        System.out.println("Total from 1 to 100 is: " +total);

        int []number1 = {1, 5, 10} ;
        int number2[] = {1,2,3,5,7,9} ;
        int totalArr1,totalArr2;

        totalArr1 = task.sumNumbersV2(number1);
        totalArr2 = task.sumNumbersV2(number2);

        System.out.println("Total arr1: " + totalArr1);
        System.out.println("Total arr2: " + totalArr2);

        Task5010.printHello(24);
        Task5010.printHello(99);
    }
    /*

    ***/
    public static void printHello(int number) {
        if (number%2 == 1 ){
            System.out.println(number +" Đây là số lẻ");
        }else{
            System.out.println(number+ " Đây là số chẵn");
        }
    }
    public int sumNumbersV1() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        return sum;
    }
    public static int sumNumbersV1a() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        return sum;
    }
    public int sumNumbersV2(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum = sum + numbers[i];
        }
        return sum;
    }
}
