import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class task5020 {
  public static void main(String[] args) throws Exception {
    task5020 doituong = new task5020();
    System.out.println("hello");
    String a = "Devcamp";
    int[] arrB = { 1, 3, 4 };
    int[] arr = { 1, 2, 3, 4, 5, 6 };
    // yêu cầu 1
    System.out.println(thuong(a));
    System.out.println(thuong(arrB));
    // yêu cầu 2
    printElement(arr, 3);
    // yêu cầu 3
    int[] arr2 = { 3, 8, 7, 6, 5, -4, -3, 2, 1 };
    bubbleSort(arr2);
    System.out.print("Mảng sau khi sắp xếp: ");
    for (int i = 0; i < arr2.length; i++) {
      System.out.print(arr2[i] + " ");
    }
    // yêu cầu 4
    System.out.println(" \n yeu cau 4");
    System.out.println(vitriphantu(arr, 3));
    System.out.println(vitriphantu(arr, 7));
    // yêu cầu 5
    System.out.println(" \n yeu cau 5");
    int[] a5 = { 1, 2, 3 };
    int[] b = { 4, 5, 6 };
    int[] c = concatenateArrays(a5, b);
    System.out.println(Arrays.toString(c));
    // yc 6
    System.out.println("yêu cầu 6");
    Object[] arrask6 = { "NaN", 0, 15, false, -22, "undefined", 47, null };

    Object[] arrask6result = doituong.task6(arrask6);

    for (int i = 0; i < arrask6result.length; i++) {
      System.out.print(arrask6result[i] + " ");
    }
    // yc 7
    System.out.println("\n yeu cau 7 \n");
    int[] arrTask7 = {2, 5, 9, 6};
    int nTask7 = 5;
    int[] resultTask7  = doituong.task7(arrTask7, nTask7);
    System.out.println(Arrays.toString(resultTask7));
    // yc8 
    System.out.println("\n yeu cau 8 \n");
    int[] task8arr = {1,2,3,4,5,6,7,8,9};
    Random rand = new Random();
    int index = rand.nextInt(task8arr.length);
    int randomElement = arr[index];
    System.out.println(randomElement);

    // y9
    System.out.println("\n yeu cau 9 \n");
    int[] arrTask9 = new int[13];
    Arrays.fill(arrTask9, 10);
    System.out.println(Arrays.toString(arrTask9));

    // y10
    System.out.println("\n yeu cau 10 \n");
    int[] arrTask10 = doituong.task10(3,27);
System.out.println(Arrays.toString(arrTask10));






  }

  public static boolean thuong(Object args) {
    return args.getClass().isArray();
  }

  // yêu cầu 2
  public static void printElement(int[] arrs, int n) {
    System.out.println("Phần tử thứ " + n + " là " + arrs[n]);
  }

  // yc 3
  public static void bubbleSort(int[] arr) {
    int n = arr.length;
    for (int i = 0; i < n - 1; i++) {
      for (int j = i + 1; j < n; j++) {
        if (arr[i] > arr[j]) {
          int temp = arr[i];
          arr[i] = arr[j];
          arr[j] = temp;
        }
      }
    }
  }

  // yêu cầu 4 viét hàm trả về vị trí thứ n trong mảng dfgfdgd
  public static int vitriphantu(int[] paramArr, int paramGiaTri) {
    int vViTri = -1;
    for (int i = 0; i < paramArr.length; i++) {
      if (paramArr[i] == paramGiaTri) {
        vViTri = i;
      }
    }

    return vViTri;
  }

  // yêu cầu 5
  public static int[] concatenateArrays(int[] a, int[] b) {
    int[] result = new int[a.length + b.length];
    System.arraycopy(a, 0, result, 0, a.length);
    System.arraycopy(b, 0, result, a.length, b.length);
    return result;
  }

  // yêu cầu 6
  public static Object[] task6(Object[] arr) {
    List<Object> filteredList = new ArrayList<>();
    for (Object obj : arr) {
      if (obj instanceof String || obj instanceof Number) {
        filteredList.add(obj);
      }
    }
    return filteredList.toArray();
  }

  // yc 7
  public static int[] task7(int[] arr, int n) {
    int[] result = new int[arr.length - 1];
    int j = 0;
    for (int i = 0; i < arr.length; i++) {
        if (arr[i] != n) {
            result[j] = arr[i];
            j++;
        }
    }
    return result;
}
  // yc 10
  public static int[] task10(int x, int y) {
    int[] arr = new int[y];
    for (int i = 0; i < y; i++) {
      arr[i] = x + i;
    }
    return arr;
  }

}
