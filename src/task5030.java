import java.util.Arrays;

public class task5030 {
    public static void main(String[] args) throws Exception {
        System.out.println("https://www.javatpoint.com/java-string " + "\n");
        System.out.println("yêu cầu 1 Kiểm tra giá trị truyền vào có phải chuỗi hay không  ");
        System.out.println("Devcamp  là chuỗi : " + task1("Devcamp"));
        System.out.println("1  là chuỗi : " + task1(1));

        System.out.println("yêu cầu 2 Cắt n phần tử đầu tiên của chuỗi");
        System.out.println(task2("Robin Singh", 4));

        System.out.println("yêu cầu 3 Chuyển chuỗi thành mảng các từ trong chuỗi");
        String[] arr3 = task3("Robin Singh");

        for (int i = 0; i < arr3.length; i++) {
            System.out.println(arr3[i]);
        }

        System.out.println("yêu cầu 4 Chuyển chuỗi về định dạng như output");
        System.out.println(task4("Robin Singh from USA"));

        System.out
                .println("yêu cầu 5 Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu tiên của mỗi từ");
        System.out.println(task5("JavaScript Exercises"));
        System.out.println(task5("JavaScript exercises"));
        System.out.println(task5("JavaScriptExercises"));

        System.out.println("yêu cầu 6 Viết hoa chữ cái đầu tiên mỗi từ  ");
        System.out.println(task6("Jjs string exercises"));

        System.out.println("yêu cầu 7 Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp");
        System.out.println(task7("haha", 6));

        System.out.println("yêu cầu 8 Đưa chuỗi thành mảng mà mỗi phần tử là n ký tự lần lượt từ chuỗi đó   ");
        System.out.println(Arrays.toString(task8("dcresource", 3)));
        System.out.println(Arrays.toString(task8("dcresource", 4)));

        System.out.println("yêu cầu 9  Đếm số lần xuất hiện của chuỗi con trong 1 chuỗi cho trước  ");
        System.out.println(task9("the quick brown fox jumps over the lazy dog", "the"));
        System.out.println("yêu cầu 10  Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác ");

    }

    // phần khai báo các method
    // task 1 Kiểm tra giá trị truyền vào có phải chuỗi hay không
    public static boolean task1(Object paramA) {
        if (paramA instanceof String) {
            return true;
        } else {
            return false;
        }

    }

    // task 2 Cắt n phần tử đầu tiên của chuỗi
    public static String task2(String paramA, int paramB) {
        String newStr = paramA.substring(0, paramB);
        return newStr;

    }

    // task 3 Chuyển chuỗi thành mảng các từ trong chuỗi
    public static String[] task3(String paramA) {
        String[] arrOfStr = paramA.split(" ");
        return arrOfStr;
    }

    // task 4 Chuyển chuỗi về định dạng như output
    public static String task4(String paramA) {
        String lowerStr = paramA.toLowerCase(); // chuyển in hoa thành chữ thường
        String hyphenStr = lowerStr.replace(" ", "-"); // chuyển toàn bộ chuỗi có khoảng trắng thành -
        return hyphenStr;
    }

    // task 5 Bỏ các khoảng trắng giữa các từ trong chuỗi, viết hoa chữ cái đầu tiên
    public static String task5(String paramA) {
        String[] words = paramA.split(" ");
        StringBuilder sb = new StringBuilder();

        for (String word : words) {
            sb.append(word.substring(0, 1).toUpperCase() + word.substring(1));
        }
        String capitalizedStr = sb.toString().trim();
        return capitalizedStr;
    }

    // task 6 Viết hoa chữ cái đầu tiên mỗi từ
    // của mỗi từ
    public static String task6(String paramA) {
        String[] words = paramA.split(" ");
        StringBuilder sb = new StringBuilder();

        for (String word : words) {
            sb.append(word.substring(0, 1).toUpperCase() + word.substring(1)).append(" ");
        }
        String capitalizedStr = sb.toString().trim();
        return capitalizedStr;
    }

    // task 7 Lặp lại n lần từ 1 từ cho trước, có khoảng trắng giữa các lần lặp
    public static String task7(String paramA, int paramN) {
        String repeatedStr = (paramA + " ").repeat(paramN).trim();
        return repeatedStr;
    }

    // task 8 Đưa chuỗi thành mảng mà mỗi phần tử là n kparamNý tự lần lượt từ chuỗi
    // đó
    public static String[] task8(String paramA, int paramN) {
        String[] arr = paramA.split("(?<=\\G.{" + paramN + "})");
        return arr;
    }
    // task 9 Đếm số lần xuất hiện của chuỗi con trong 1 chuỗi cho trước

    public static int task9(String paramA, String paramB) {
        int count = 0;
        int index = paramA.indexOf(paramB);

        while (index != -1) {
            count++;
            index = paramA.indexOf(paramB, index + 1);
        }
        return count;
    }
    // task 10 Thay n ký tự bên phải của một chuỗi bởi 1 chuỗi khác

}
